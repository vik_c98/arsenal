"""
Word Embeddings using Machine Learning
"""
from nltk import word_tokenize, sent_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem.wordnet import WordNetLemmatizer
import nltk
import gensim
from gensim import corpora, models, similarities
from sentimentAnalysis import *
from NetworkGraph import returnTopReplied

# reference : https://www.datacamp.com/community/tutorials/text-analytics-beginners-nltk
def deleteStopWords(text):
    """
    function deleteStopWords gets rid of the Stopwords in a given text and returns the result as a list of tokens
    :param text  the text we are interested in
    :raise:
    :pre: nltk package installed and imported
    :return: return a list of filtered list of words
    :time complexity : O(n)
    """
    text = text.lower()
    tokenized_text = nltk.word_tokenize(text)
    stop_words = set(stopwords.words("english"))
    filtered_list = []
    for word in tokenized_text:
        if word not in stop_words:
            filtered_list.append(word)

    return filtered_list


# affixes. For example, connection, connected, connecting word reduce to a common word "connect".
def Stemming(filtered_list):
    """
     Stemming is a process of linguistic normalization, which reduces words to their word root word or chops off the derivational
     :param filtered_list from the result of function deleteStopWords
     :raise:
     :pre: nltk package installed and imported, function deleteStopWords ready to use
     :return: return a list of filtered list of words where they are only in their root form
     :time complexity : O(n)
     """
    PS = PorterStemmer()
    stemmedWords = []
    for word in filtered_list:
        stemmedWords.append(PS.stem(word))

    return stemmedWords

def Lemmatization(str,choice):
    """
     Stemming is a process of linguistic normalization, which changes words to the given choice like v, n or adj and etc.
     :param str that we are intersted in
     :raise:
     :pre: nltk package installed and imported
     :return: changes words to the given choice like v, n or adj and etc and returns it
     :time complexity : O(n)
     """
    Lem = WordNetLemmatizer()
    return Lem.lemmatize(str,choice)


def indexPos(itr):
    headerRow = []
    count = 0
    index_of_fulltext = 0
    for index in itr:
        if count == 0:
            headerRow = index
            print(headerRow)
            count +=1
        else:
            break
    for i in range(len(headerRow)):
        if headerRow[i] == "full_text":
            index_of_fulltext = i
    return index_of_fulltext

def WordEmbeddings_main(choiceOfData):
    Full_texts = []
    FTS = fetchdata(choiceOfData)
    data = fetch(choiceOfData)
    originialTweets, total_original = filterTweets(FTS)
    for record in originialTweets:
        Full_texts.append(record[0])



    corpus = Full_texts
    tok_corp = [nltk.word_tokenize(sent) for sent in corpus]
    model = gensim.models.Word2Vec(tok_corp, min_count=2, size=300)
    #
    # # model.save('testmodel')
    # # model = gensim.models.Word2Vec.load('test_model')

    choicesofcategory = ["government", "media"]
    total_toplist = []
    for each_categroy in choicesofcategory:
        total_toplist = total_toplist + returnTopReplied(each_categroy, data)

    resultlist =[]
    for keyterm in total_toplist:
        resultlist = resultlist + [keyterm]+model.most_similar(positive=[keyterm], topn=5)
    return resultlist
    # model.most_similar([vector])

# def wordembedding_main(choiceOfdata):
#     result = []
#     data = fetch(choiceOfdata)





# print(WordEmbeddings_main("Irma"))

# t = "@realDonaldTrump you're an idiot. Be a leader"
# print(deleteStopWords(t))
#Problem:
#@ is treated as single token