
from textblob  import TextBlob
from Extractdata import *
import re
from Dict import *
import ast
# The line of code to remove urls within a string is taken from :https://stackoverflow.com/questions/11331982/how-to-remove-any-url-within-a-string-in-python


def indexPos(itr):
    """
    This function is used to return the indexes of the needed columns from database
    :param: data iterator of the data in the database
    :raise:
    :pre: The data iterator exists (or has been declared before)
    :return: return the indexes of the needed columns from database
    :Time complexity: O(n), where n denote the number of line in the csv file.
    """
    headerRow = []
    count = 0
    index_of_fulltext = 0
    index_of_retweetedstatus = 0
    index_of_user_screenname = 0
    index_of_metioned_ids = 0
    for index in itr:
        if count == 0:
            headerRow = index
            count +=1
        else:
            break
    for i in range(len(headerRow)):
        if headerRow[i] == "full_text":
            index_of_fulltext = i
        elif headerRow[i] == "retweeted_status":
            index_of_retweetedstatus = i
        elif headerRow[i] == "user_name":
            index_of_user_screenname = i
        elif headerRow[i] == "mentioned_ids":
            index_of_metioned_ids = i
    return index_of_fulltext,index_of_retweetedstatus,index_of_user_screenname,index_of_metioned_ids

def fetchdata(choiceOfData):
    """
    This function fetches the data in the form of a list from the database and then extracts
    only the fulltext, retweetedstatus, user_screenname, metioned_ids key attributes and save it into a list called full_texts
    :param: choice of database
    :raise:
    :pre: fetch () is ready to use
    :return: a list of tweets containing only the fulltext, retweetedstatus, user_screenname, metioned_ids key attributes
    :Time complexity: O(n)
    """
    total_numofTweets = 0
    text_itr = fetch(choiceOfData)
    full_texts = []
    index_of_fulltext, index_of_retweetedstatus, index_of_user_screenname, index_of_metioned_ids = indexPos(text_itr)
    count = 0
    for tweets in text_itr:
        total_numofTweets+=1
        if count>0:
            a = tweets[index_of_metioned_ids]
            if len(a)>2:
                a = a[1:len(a)-1]
                a = ast.literal_eval(a)
                if checkisTuple(a) == True:
                    names = []
                    for i in range(len(a)):
                        names.append(a[i]["screen_name"])
                    full_texts.append([tweets[index_of_fulltext], tweets[index_of_retweetedstatus],tweets[index_of_user_screenname], names])

                else:
                    name = []
                    name.append(a["screen_name"])
                    full_texts.append([tweets[index_of_fulltext],tweets[index_of_retweetedstatus], tweets[index_of_user_screenname], name])

            else:
                full_texts.append([tweets[index_of_fulltext],tweets[index_of_retweetedstatus], tweets[index_of_user_screenname], []])

        count+=1
    return full_texts


def filterTweets(full_texts):
    """
    This function filters out retweets and returns  only a list of original tweets
    :param: the resulting list of tweets after all the above functions to process
    :raise:
    :pre: The resulting list of tweets is url-free and processed collectedly
    :return: returns  only a list of original tweets
    :Time complexity: O(n)
    """
    filtered_tweet = []
    total_originallTweets = 0
    for tweet in full_texts:
        if tweet[1] == 'False':
            filtered_tweet.append((tweet[0],tweet[2],tweet[3]))
            total_originallTweets+=1
    return filtered_tweet,total_originallTweets


def removeURL(FTs):
    """
    This function is used to get rid of the urls in the full_text column
    :param:
    :raise:
    :pre: The data iterator exists (or has been declared before)
    :return: return the indexes of the needed columns from database
    :Time complexity: O(n), where n denote the number of line in the csv file.
    """
    for i in range(len(FTs)):
        FTs[i] = (re.sub(
            r'''(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))''',
            " ", FTs[i][0]),FTs[i][1],FTs[i][2])

    return FTs


def extractMedia_text(FTS):
    """
    This function extracts all the tweets on media (whose screen name match those we predefined in media_dict) and returns it as it list
    as well as the number of original tweets there is in that list
    :param: the resulting list of tweets after all the above functions to process
    :raise:
    :pre: The resulting list of tweets is url-free and processed collectedly
    :return: and returns all the tweets on media as it list as well as the number of original tweets there is in that list
    :Time complexity: O(n^2)
    """
    total_nonRTmediatweets =0
    new_FTS = removeURL(FTS)
    media_name = media_dict()
    media_text = []
    for i in range(len(new_FTS)):
        if new_FTS[i][2] != []:
            for j in range(len(new_FTS[i][2])):
                if checkexist(media_name, new_FTS[i][2][j]) == 1:
                    media_text.append((new_FTS[i][0],new_FTS[i][2][j]))
                    total_nonRTmediatweets+=1
                    break
    return media_text,total_nonRTmediatweets


def extractGov_text(FTS):
    """
    This function extracts all the tweets on government (whose screen name match those we predefined in government_dict) and returns it as it list
    as well as the number of original tweets there is in that list
    :param: the resulting list of tweets after all the above functions to process
    :raise:
    :pre: The resulting list of tweets is url-free and processed collectedly
    :return: and returns all the tweets on government as it list as well as the number of original tweets there is in that list
    :Time complexity: O(n^2)
    """
    total_nonRTgovTweets = 0
    new_FTS = removeURL(FTS)
    gov_name = gov_dict()
    gov_text = []
    for i in range(len(new_FTS)):
        # print(new_FTS[i])
        if new_FTS[i][2] != []:
            for j in range(len(new_FTS[i][2])):
                if checkexist(gov_name, new_FTS[i][2][j]) == 1:
                    gov_text.append((new_FTS[i][0], new_FTS[i][2][j]))
                    total_nonRTgovTweets += 1
                    break
    return gov_text, total_nonRTgovTweets


def checkisTuple(str):
    """
    This function checks whether a given list of mentioned_ids contains more than 1 screennames. Return True if yes else False
    :param: the list of mentioned_ids
    :raise:
    :pre: the list of mentioned_ids is in correct format
    :return: Return True if yes else False
    :Time complexity: O(1)
    """
    try:
        value = str[0]["screen_name"]
        return True
    except:
        return False


def sentimentanalysis(origintweets):
    """
    This function takes in a list of tweets and catagorize them into positive and negative and finally returns the count for each
    :param: a list of oirginal tweets
    :raise:
    :pre: the list only contains original tweets and is successfully processed with all the previous functions
    :return: the number of positive & negative tweets as ints
    :Time complexity: O(n)
    """
    pos_count = 0
    neg_count = 0
    for i in range (len(origintweets)):
        tb = TextBlob(origintweets[i][0])
        if tb.polarity > 0:
            pos_count +=1
            # print(origintweets[i][0],tb.polarity,tb.subjectivity)
        elif tb.polarity < 0:
            neg_count +=1

    return pos_count,neg_count


def displayData(choiceOfData):
    """
    This function is the main function of this file where all the actions happen. It will display the findings/results of sentiment analysis
    :param: None
    :raise:
    :pre: all the functions used is ready and textblob installed and imported
    :return: the result/findings of sentiment analysis
    :Time complexity: O(n^2)
    """
    strout = "Result for sentiment analysis for " + choiceOfData + ":\n"
    Fulltexts = fetchdata(choiceOfData)

    originialTweets, total_original = filterTweets(Fulltexts)
    nonRT_mediaTweets, total_mediaOriginal = extractMedia_text(originialTweets)
    # objective 1
    strout = strout + "the percentagle of media-related tweets out of all the original tweets is : %.02f%%" % ((total_mediaOriginal / total_original) * 100) + '\n'
    # print("the percentagle of media-related tweets out of all the original tweets is : %.02f%%" % ((total_mediaOriginal / total_original) * 100))

    # objective 2
    total_pos_media, total_neg_media = sentimentanalysis(nonRT_mediaTweets)
    strout = strout + "The percentage of positive tweets on media coverage is: %.02f%%" % ((total_pos_media / total_mediaOriginal) * 100) + '\n'
    strout = strout + "The percentage of negative tweets on media coverage is: %.02f%%" % ((total_neg_media / total_mediaOriginal) * 100) + '\n'
    # print("The percentage of positive tweets on media coverage is: %.02f%%" % ((total_pos_media / total_mediaOriginal) * 100))
    # print("The percentage of negative tweets on media coverage is: %.02f%%" % ((total_neg_media / total_mediaOriginal) * 100))

    # objective 3
    nonRT_govTweets, total_govOriginal = extractGov_text(originialTweets)
    strout = strout + "the percentagle of government-related tweets out of all the original tweets is : %.02f%%" % ((total_govOriginal / total_original) * 100) + '\n'
    # print("the percentagle of government-related tweets out of all the original tweets is : %.02f%%" % ((total_govOriginal / total_original) * 100))

    # objective 4
    total_pos_gov, total_neg_gov = sentimentanalysis(nonRT_govTweets)
    strout = strout + "The percentage of positive tweets on government is: %.02f%%" % ((total_pos_gov / total_govOriginal) * 100) + '\n'
    strout = strout + "The percentage of negative tweets on government is: %.02f%%" % ((total_neg_gov / total_govOriginal) * 100) + '\n'
    # print("The percentage of positive tweets on government is: %.02f%%" % ((total_pos_gov / total_govOriginal) * 100))
    # print("The percentage of negative tweets on government is: %.02f%%" % ((total_neg_gov / total_govOriginal) * 100))

    return strout
# displayData("Irma")
# Fulltexts = fetchdata("Irma")
# originialTweets, total_original = filterTweets(Fulltexts)
