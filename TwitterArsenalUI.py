import matplotlib
matplotlib.use('TkAgg')
from tkinter import *
from tkinter import ttk
from Integration import *

class TwitterArsenalUI:
    def __init__(self,master):
        self.master = master
        self.width = 300
        self.height = 300
        self.OpenYet = False
        self.filename = None
        self.count = 0
        frame = Frame(master,width = self.width,height = self.height)
        frame.pack()
        frame.pack_propagate(0)  #prevent frame to be shrinking to fit size of button

        self.sfbutton = ttk.Button(frame,text="Harvey",command=self.display_results_for_Harvey)
        ttk.Style().configure('green/black.TButton')
        self.sfbutton.pack()

        self.cubutton = ttk.Button(frame,text="Irma", command =self.display_results_for_Irma)
        ttk.Style().configure('green/black.TButton')
        self.cubutton.pack()

    def display_results_for_Harvey(self):
        display_all_results("Harvey")

    def display_results_for_Irma(self):
        display_all_results("Irma")


root = Tk()
T = TwitterArsenalUI(root)
root.mainloop()
