from Extractdata import fetch
from NetworkGraph import indexPos,returnTopReplied
from Dict import *

"""
The five graph metrics we picked are:
    1.The Total number of Vertices and Total number of Edges of the Social Network Graph 
    2.The average degree of the Social Network Graph 
    3.The density of the  of the Social Network Graph 
    4.The Maximum_Edge_In_A_Connected_Component of the Social Network Graph 
    5.The Modularity of the Social Network Graph
"""


def Top5MedText(choiceOfData):
    """
    function used to create a text file to contain all the user connection(reply) related to Top 5 media users.
    :param choice of the name of database
    :pre: all data have been stored in the database and fetch () is ready to use,
    :return: create a text file to contain all the user connection(reply) related to Top 5 media.
    :time complexity : O(n) n being number of records in the database
    """
    data = fetch(choiceOfData)
    Top5Med = returnTopReplied("media", data)
    index_of_user_name, index_of_user_id, index_of_in_reply_to_user_id, index_of_in_reply_to_screen_name = indexPos(
        data)
    count = 0
    f = open("GraphMetricMedia.txt", "w+")
    for tweet in data:
        if count != 0:
            if tweet[index_of_in_reply_to_user_id] != "" and tweet[index_of_user_id] != tweet[
                index_of_in_reply_to_user_id]:
                if checkexist(media_dict(), tweet[index_of_user_name]) == 1 or checkexist(media_dict(), tweet[
                    index_of_in_reply_to_screen_name]) == 1:
                    if tweet[index_of_in_reply_to_screen_name] in Top5Med:
                        UserName = str(tweet[index_of_user_name])
                        in_reply_to_screen_name = str(tweet[index_of_in_reply_to_screen_name])
                        NewUserName = UserName.replace(" ", "_")
                        NEW_in_reply_to_screen_name = in_reply_to_screen_name.replace(" ", "_")
                        stringToWrite = NewUserName + " ," + NEW_in_reply_to_screen_name + "\n"
                        f.write(stringToWrite)
        count += 1


def Top5GovText(choiceOfData):
    """
    function used to create a text file to contain all the user connection(reply) related to Top 5 government users.
    :param: choice of the name of database
    :pre: all data have been stored in the database and fetch () is ready to use,
    :return: create a text file to contain all the user connection(reply) related to Top 5 government.
    :time complexity : O(n) n being number of records in the database
    """
    data = fetch(choiceOfData)
    Top5Gov = returnTopReplied("government", data)
    index_of_user_name, index_of_user_id, index_of_in_reply_to_user_id, index_of_in_reply_to_screen_name = indexPos(
        data)
    count = 0
    f = open("GraphMetricGovernment.txt", "w+")
    for tweet in data:
        if count != 0:
            if tweet[index_of_in_reply_to_user_id] != "" and tweet[index_of_user_id] != tweet[
                index_of_in_reply_to_user_id]:
                if checkexist(gov_dict(), tweet[index_of_user_name]) == 1 or checkexist(gov_dict(), tweet[
                    index_of_in_reply_to_screen_name]) == 1:
                    if tweet[index_of_in_reply_to_screen_name] in Top5Gov:
                        UserName = str(tweet[index_of_user_name])
                        in_reply_to_screen_name = str(tweet[index_of_in_reply_to_screen_name])
                        NewUserName = UserName.replace(" ", "_")
                        NEW_in_reply_to_screen_name = in_reply_to_screen_name.replace(" ", "_")
                        stringToWrite = NewUserName + " ," + NEW_in_reply_to_screen_name + "\n"
                        f.write(stringToWrite)
        count += 1

def Display_Average_Degree_And_Total_Vertices_And_Edges():
    """
    function used to display two graph metrics: average degree and Total number of vertices and edges
    :param: None
    :raise:when the user doesn't in the dictionary which is used to contain the number of vercties(users) and edges(records in the text file)
    :pre: the textfiles that contain all the user connection(reply) related to Top 5 media and Top 5 government are exist
    :param None
    :return: display two graph metrics: average degree and Total number of vertices and edges.
    :time complexity : O(T+K) T being number of records in the texfile for top 5 government, K being number of records in the textfile for top 5 media.
    """
    fgov = open("GraphMetricGovernment.txt","r")
    fmed = open("GraphMetricMedia.txt","r")
    total_number_of_edges_gov = 0
    total_number_of_edges_med = 0
    total_number_of_vertices_gov = 0
    total_number_of_vertices_med = 0
    dictForGov = dict()
    dictForMed = dict()
    for lines in fgov:
        total_number_of_edges_gov+=1
        lines = lines.split(" ,")
        try:
            v=dictForGov[lines[0]]
        except KeyError:
            dictForGov[lines[0]]=1
            total_number_of_vertices_gov+=1
        try:
            v=dictForGov[lines[1]]
        except KeyError:
            dictForGov[lines[1]]=1
            total_number_of_vertices_gov+=1
    fgov.close()

    for lines in fmed:
        total_number_of_edges_med+=1
        lines = lines.split(" ,")
        try:
            v=dictForMed[lines[0]]
        except KeyError:
            dictForMed[lines[0]]=1
            total_number_of_vertices_med+=1
        try:
            v=dictForMed[lines[1]]
        except KeyError:
            dictForMed[lines[1]]=1
            total_number_of_vertices_med+=1
    fmed.close()

    strout = "The total number of vertices for Graph of Media is : " + str(total_number_of_vertices_med)+"\n"
    strout = strout + "The total number of Edges for Graph of Media is : " + str(total_number_of_edges_med)+"\n"
    strout = strout + "The average degree for the Social Network Graph of Top 5 Media Tweet accounts is %.02f%%" % (
                total_number_of_edges_med / total_number_of_vertices_med) + "\n"

    strout = strout + "The total number of vertices for Graph of Government is : " + str(total_number_of_vertices_gov)+"\n"
    strout = strout + "The total number of Edges for Graph of Government is : " + str(total_number_of_edges_gov)+"\n"
    strout = strout + "The average degree for the Social Network Graph of Top 5 Government Tweet accounts is %.02f%%" % (total_number_of_edges_gov/total_number_of_vertices_gov)+'\n'
    return strout

def Display_Density(choiceOfData):
    """
    function used to display one graph metric: the density (edges percentage for top 5 media and governmnent users
    :param: choice of the name of database
    :pre: the textfiles that contain all the user connection(reply) related to Top 5 media and Top 5 government are exist
    :param None
    :return: display one graph metrics: the density
    :time complexity : O(T+K) T being number of records in the texfile for top 5 government, K being number of recors in the textfile for top 5 media.
    """
    fmed = open("GraphMetricMedia.txt", "r")
    totalNumberOfEdges = 0
    data = fetch(choiceOfData)
    Top5Med = returnTopReplied("media", data)


    densityForMed = [0.0] * len(Top5Med)
    for lines in fmed:
        lines = lines.strip().split(" ,")
        totalNumberOfEdges += 1
        for i in range(len(Top5Med)):
            if str(lines[1]) == Top5Med[i]:
                densityForMed[i]+=1.0

    stroutforMed = "The density distribution for Module Media shown as below: \n"
    for i in range(len(densityForMed)):
        densityForMed[i]= densityForMed[i]/totalNumberOfEdges
        stroutforMed = stroutforMed +"The density of " + Top5Med[i] +" is " + str(densityForMed[i]) +"\n"
    fmed.close()
    # print(stroutforMed)


    fgov = open("GraphMetricGovernment.txt", "r")
    totalNumberOfEdges = 0
    Top5Gov = returnTopReplied("government", data)

    densityForGov = [0.0] * len(Top5Gov)
    for lines in fgov:
        lines = lines.strip().split(" ,")
        totalNumberOfEdges += 1
        for i in range(len(Top5Gov)):
            if str(lines[1]) == Top5Gov[i]:
                densityForGov[i] += 1.0


    stroutforGov = "The density distribution for Module Government shown as below: \n"
    for i in range(len(densityForGov)):
        densityForGov[i] = densityForGov[i] / totalNumberOfEdges
        stroutforGov = stroutforGov + "The density of " + Top5Gov[i] + " is " + str(densityForGov[i]) + "\n"
    fgov.close()
    return stroutforGov,stroutforMed

def Display_Maximum_Edge_In_A_Connected_Component(choiceOfData):
    """
    function used to display one graph metric: the count of total edges in the largest connected component
    :param choice of the name of database
    :pre: the textfiles that contain all the user connection(reply) related to Top 5 media and Top 5 government are exist
    :param None
    :return: display one graph metrics: the count of total edges in the largest connected component
    :time complexity : O(T+K) T being number of records in the texfile for top 5 government, K being number of recors in the textfile for top 5 media.
    """
    data = fetch(choiceOfData)
    Top5Med = returnTopReplied("media", data)
    Top5Gov = returnTopReplied("government",data)
    fmed = open("GraphMetricMedia.txt", "r")
    fgov = open("GraphMetricGovernment.txt", "r")
    MaximumEdgeforMed = 0
    MaximumEdgeforGov = 0
    for lines in fmed:
        lines = lines.strip().split(" ,")
        if lines[1]==Top5Med[0]:
            MaximumEdgeforMed+=1

    fmed.close()

    for lines in fgov:
        lines = lines.strip().split(" ,")
        if lines[1]==Top5Gov[0]:
            MaximumEdgeforGov+=1
    fgov.close()
    strout = "The maximum edge in the largest connected media-related graph is: "  + Top5Med[0] + " with " + str(MaximumEdgeforMed) + " edges\n"
    strout = strout + "The maximum edge in the largest connected government-related graph is: "  + Top5Gov[0] + " with " + str(MaximumEdgeforGov) + " edges\n"
    return strout

def Display_Modularity_Between_Media_And_Government(choiceOfData):
    """
    Display_Modularity_Between_Media_And_Government function calculates the metric Modularity of the Social Network Graph created earlier
    :param: choice of the name of database
    :raise:
    :pre: fetch() ready to use
    :return: displays the Modularity_Between_Media_And_Government of the given Social Network Graph to users
    :time complexity : O(n) n being the size of the txt files combined
    """
    data = fetch(choiceOfData)
    Top5Med = returnTopReplied("media", data)
    Top5Gov = returnTopReplied("government",data)
    fmed = open("GraphMetricMedia.txt", "r")
    fgov = open("GraphMetricGovernment.txt", "r")
    Modularity = 0
    for lines in fmed:
        lines = lines.strip().split(" ,")
        if lines[0] in Top5Gov:
            Modularity+=1
    fmed.close()
    for lines in fgov:
        lines = lines.strip().split(" ,")
        if lines[0] in Top5Med:
            Modularity+=1
    fgov.close()
    return "The modularity Between The goverment and the media is "+ str(Modularity)+"\n"


def GraphMetrics_main(choiceOfData):
    Top5MedText(choiceOfData)
    Top5GovText(choiceOfData)
    strout = "Results for Graph Metrics: \n"
    strout = strout + str(Display_Average_Degree_And_Total_Vertices_And_Edges())
    stroutforGov,stroutforMed=Display_Density(choiceOfData)
    strout = strout + str(stroutforGov)+"\n"+str(stroutforMed)
    strout = strout + str(Display_Maximum_Edge_In_A_Connected_Component(choiceOfData))
    strout = strout + str(Display_Modularity_Between_Media_And_Government(choiceOfData))
    return strout


# main("Irma")
# Display_Average_Degree_And_Total_Vertices_And_Edges()
# DisplayDensity()
# Display_Maximum_Edge_In_A_Connected_Component()
# Display_Modularity_Between_Media_And_Government()




