# import nltk
# from nltk.tokenize import word_tokenize
# from nltk.tag import pos_tag
# from nltk.chunk import conlltags2tree, tree2conlltags,ne_chunk
# from pprint import pprint
# """
# The code contained in this url has been taken into consideration : https://towardsdatascience.com/named-entity-recognition-with-nltk-and-spacy-8c4a7d88e7da
# """
#
# ex = 'European authorities fined Google a record $5.1 billion on Wednesday for abusing its power ' \
#      'in the mobile phone market and ordered the company to alter its practices'
# def preprocess(sent):
#     sent = nltk.word_tokenize(sent)
#     sent = nltk.pos_tag(sent)
#     return sent
#
# sent = preprocess(ex)
# print(sent)
# pattern = 'NP: {<DT>?<JJ>*<NN>}'
# # words like 'a', 'the', 'some', 'this' and 'each belong to the tag DT,
# # this line of code is trying to match all the possible noun phrases(single form not plural)
# cp = nltk.RegexpParser(pattern)
# cs = cp.parse(sent)
# print(cs)
#
#
# iob_tagged = tree2conlltags(cs)
# pprint(iob_tagged)
# # see this link for explanation : https://www.nltk.org/book/ch07.html
#
# ne_tree = ne_chunk(pos_tag(word_tokenize(ex)))
# print(ne_tree)

import spacy
from spacy import displacy
from collections import Counter
from pprint import pprint
import en_core_web_sm
from NetworkGraph import returnTopReplied
from Extractdata import fetch
import mysql.connector
from mysql.connector import errorcode
import ast
from sentimentAnalysis import checkisTuple

def NamedEntityRecognition(text):
    """
    # NORD -- nationalities or religious or political groups
    # ORG  -- organzation
    # money -- money
    # DATE -- date
       NamedEntityRecognition function takes in a given test and performs NamedEntityRecogntion using the spacey library which we compared with NLTK
       and apparently Spacy has better accuracy in this task.
        :param text user given input as text
        :raise:
        :pre: spacey library installed and imported, Counter module imported, pprint(pretty-print) module imported and en_core_web_sm doucmentations for spacey installed and imported
        :return: the result of NamedEntityRecognition on the given text
        :time complexity : O(n) n being the size of the documentation that spacy uses to perform this task
    """
    nlp = en_core_web_sm.load()
    doc = nlp(text)
    return [(X.text, X.label_) for X in doc.ents]


def extractRealNames(data,screenname):
    data = ast.literal_eval(data[0][0])
    for i in range(len(data)):
        if data[i]["screen_name"]==screenname:
            return data[i]["name"]
    if data=="[]":
        return False




    # a = (a[0][0])[1:len(a[0][0]) - 1]
    # print(len(a.split(", {")))
    # if len(a) == 0:
    #     return False
    # a = ast.literal_eval(a)
    # if a["screen_name"] == screenname:
    #     return a["name"]

# def extractRealName(a,)



# def checkisTuple(str):
#     """
#     This function checks whether a given list of mentioned_ids contains more than 1 screennames. Return True if yes else False
#     :param: the list of mentioned_ids
#     :raise:
#     :pre: the list of mentioned_ids is in correct format
#     :return: Return True if yes else False
#     :Time complexity: O(1)
#     """
#     try:
#         value = str[0]["screen_name"]
#         return True
#     except:
#         return False


def get_realName(list_of_user_screenname,choiceOfdata):
    try:
        cnn = mysql.connector.connect(
            user='root',
            password='08110320hjbcy',
            host='localhost',
            database='Arsenal')

        cursor = cnn.cursor()
        listOfQueries=["a"]*len(list_of_user_screenname)
        listOfResults = ["a"]*len(list_of_user_screenname)
        for index in range(len(list_of_user_screenname)):
            listOfQueries[index]="SELECT mentioned_ids FROM " +  str(choiceOfdata) + " where in_reply_to_screen_name = " + "'" +str(list_of_user_screenname[index]) + "'" + " and " + "mentioned_ids <> '[]' " + "and " +"user_name <> " + "'" + str(list_of_user_screenname[index]) + "'" + " limit 1;"
        for i in range(len(listOfQueries)):
            cursor.execute(listOfQueries[i])
            data = cursor.fetchall()
            data = extractRealNames(data, list_of_user_screenname[i])
            if data == None:
                listOfResults[i] = list_of_user_screenname[i]
            else:
                listOfResults[i] = data
        cursor.close()
        return listOfResults
    except mysql.connector.Error as e:
        if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            raise StopIteration("Something is wrong with username or Password")

        elif e.errno == errorcode.ER_BAD_DB_ERROR:
            raise StopIteration("Database Does not exist")
        else:
            raise StopIteration(e)

def NamedEntityRecognition_main(choiceOfdata):
    result = []
    data = fetch(choiceOfdata)
    choicesofcategory = ["government","media"]
    total_toplist = []
    for each_categroy in choicesofcategory:
        total_toplist = total_toplist + returnTopReplied(each_categroy,data)
    listOfRealNames=get_realName(total_toplist,choiceOfdata)
    # print(listOfRealNames)
    for record in listOfRealNames:
        result = result + NamedEntityRecognition(record)
    return result

# text1 = "Donald J.Trump is the biggest jerk of any U.S. presidents of all times by CNN."
# text2 = "Donald Trump is the biggest jerk of any U.S. presidents of all times by CNN."
# print(NamedEntityRecognition(text1) + NamedEntityRecognition(text2))
# print(NamedEntityRecognition(text1))
# print(NamedEntityRecognition_main("Irma"))




# a = get_realName(["NASA"],"irma")
# print(a)
# a = [("[{'id': 11348282, 'id_str': '11348282', 'indices': [0, 5], 'name': 'NASA', 'screen_name': 'NASA'}, {'id': 24919888, 'id_str': '24919888', 'indices': [6, 16], 'name': 'NASA Earth', 'screen_name': 'NASAEarth'}]",)]
# print(a[0][0])

# a = [{'id': 25073877, 'id_str': '25073877', 'indices': [0, 16], 'name': 'Donald J. Trump', 'screen_name': 'realDonaldTrump'}, {'id': 2933862481, 'id_str': '2933862481', 'indices': [17, 28], 'name': 'Liberty Justice', 'screen_name': 'MLovesmore'}]
# # a = (a[0][0])[1:len(a[0][0])-1]
# print(a)
# a = ast.literal_eval(a)
# # print(a[0]["name"])

# print(a)
# print(extractRealNames(a,"realDonaldTrump"))
# a = ast.literal_eval(a[0])
# print(a[0]["name"])

# NamedEntityRecognition_main("Harvey")