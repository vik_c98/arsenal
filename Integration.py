from jsonToCsv import *
from csvToMySQL import *
from sentimentAnalysis import *
from Dict import *
from extension import *
from Extractdata import *
from GraphMetrics import *
from NetworkGraph import *
from NamedEntityRecognition import *
from WordEmbeddings import *
import time

def display_all_results(choiceOfdata):
    if choiceOfdata != "Harvey" and choiceOfdata != "Irma":
        raise StopIteration("Invalid choice of data, choice has to be either Harvey or Irma, the first letter should be capitalized!")

    start_time = time.time()
    #step 1 execute jsonToCsv main function.
    jsonToCsv_main(choiceOfdata)

    end1 = time.time()
    time_for_jsonToCsv_main = end1 - start_time

    #step 2 execute csvToMySQL main fucntion.
    buildConnectionDb(choiceOfdata)

    end2 = time.time()

    time_for_buildConnectionDb = end2 - end1

    #step 3 execute displayData
    resultforSentimentAnalyisis =displayData(choiceOfdata)

    end3 = time.time()

    time_for_SentimentAnalyisis = end3 - end2
    #step 4 execute NetworkGraph
    createNetworkgraph(choiceOfdata)
    file = open(str(choiceOfdata)+"Findings/results", "w+")
    file.write(resultforSentimentAnalyisis)
    file.write("\n\n\n\n")

    end4 = time.time()
    time_for_createNetworkgraph = end4 - end3

    #step 5 execute GraphMetrics
    stroutGraphMetrics = GraphMetrics_main(choiceOfdata)
    file.write(stroutGraphMetrics)
    file.write("\n\n\n\n")

    end5 =time.time()
    time_for_Graphmetrics = end5-end4

    #step 6 execute named entity recoginition
    resultforNER = NamedEntityRecognition_main(choiceOfdata)
    resultforNERstr = "Results for named entity recognition as below: \n"
    for term in resultforNER:
        resultforNERstr = resultforNERstr + "("+str(term[0])+","+str(term[1])+")" + '\n'
    file.write(resultforNERstr)
    file.write("\n\n\n\n")

    end6 = time.time()
    time_for_Named_entity_recognition=end6-end5

    #step 7 execute word embedding
    resultforword_embedding = WordEmbeddings_main(choiceOfdata)
    resultforWEstr = "Results for word embedding as below: \n"
    space = 6

    for i in range(len(resultforword_embedding)):
        if i%space ==0:
            resultforWEstr = resultforWEstr + resultforword_embedding[i]+": "
        else:
            resultforWEstr = resultforWEstr + "(" + resultforword_embedding[i][0] +"," + str(resultforword_embedding[i][1]) + ")"+"\t"
            if i%space==space-1:
                resultforWEstr = resultforWEstr + '\n'

    file.write(resultforWEstr)
    file.write("end of file")
    file.close()

    end7 = time.time()
    time_for_wordembedding = end7 -end6

    strout = "The time taken to run the script jsonToCsv.py is: "
    strout = strout + str(round(time_for_jsonToCsv_main,2)) +" seconds \n"

    strout = strout + "The time taken to run the script csvToMySQL.py is: "
    strout = strout + str(round(time_for_buildConnectionDb,2)) +" seconds \n"

    strout = strout + "The time taken to run the script sentimentAnalyisis.py is: "
    strout = strout + str(round(time_for_SentimentAnalyisis,2)) + " seconds \n"

    strout = strout + "The time taken to run the script NetworkGraph.py is: "
    strout = strout + str(round(time_for_createNetworkgraph,2)) + " seconds \n"

    strout = strout + "The time taken to run the script GraphMetrics.py is: "
    strout = strout + str(round(time_for_Graphmetrics,2)) + " seconds \n"

    strout = strout + "The time taken to run the script NamedEntityRecognition.py is: "
    strout = strout + str(round(time_for_Named_entity_recognition,2)) + " seconds \n"

    strout = strout + "The time taken to run the script wordembedding.py is: "
    strout = strout + str(round(time_for_wordembedding,2)) + " seconds \n"
    print(strout)

# display_all_results("Irma")

