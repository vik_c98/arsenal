import unittest
from csvToMySQL import collectHeaderRow,returnIterator,dropTable,createTable,insertTable
from jsonToCsv import calcsvlines
import mysql
import mysql.connector
from extension import checkextension

class Test_jsonToCsv(unittest.TestCase):

    def test_collectHeaderRow(self):
        Test_case = ['retweeted_status', 'tweet_iD', 'mentioned_ids', 'created_at', 'user_id', 'user_name', 'full_text', 'retweet_count', 'in_reply_to_user_id', 'in_reply_to_screen_name']
        self.assertEqual(Test_case,collectHeaderRow("Irma.csv"),"failed at collectHearderRow()")
        print("All Test cases pasted for test_collectHeaderRow()!")

    def test_returnIterator(self):
        data = returnIterator("Irmacleanup.csv")
        count_foritreator = 0
        for record in data:
            count_foritreator+=1
        self.assertEqual(count_foritreator,calcsvlines("Irmacleanup.csv"))
        print("All Test cases pasted for test_returnIterator()!")

    def test_dropTable(self):
        cnn = mysql.connector.connect(
            user='root',
            password='08110320hjbcy',
            host='localhost',
            database='Arsenal')
        cursor = cnn.cursor()
        self.assertEqual("DROP TABLE irma",dropTable(cursor,"Irma"),"failed at dropTable()")
        cursor.close()
        print("All Test cases pasted for test_dropTable()!")

    def test_createTable(self):
        cnn = mysql.connector.connect(
            user='root',
            password='08110320hjbcy',
            host='localhost',
            database='Arsenal')
        cursor = cnn.cursor()
        test_case = "CREATE TABLE irma (retweeted_status VARCHAR(255),tweet_iD VARCHAR(255),mentioned_ids TEXT,created_at VARCHAR(255),user_id VARCHAR(255),user_name VARCHAR(255),full_text TEXT,retweet_count VARCHAR(255),in_reply_to_user_id VARCHAR(255),in_reply_to_screen_name VARCHAR(255))"
        self.assertEqual(test_case,createTable(cursor,"Irma"),"failed at createTable()")
        cursor.close()
        print("All Test cases pasted for test_createTable()!")

    def test_insertTable(self):
        cnn = mysql.connector.connect(
            user='root',
            password='08110320hjbcy',
            host='localhost',
            database='Arsenal')
        cursor = cnn.cursor()
        pre, ext = checkextension("Irma")
        data = returnIterator(pre + "cleanup.csv")
        testing_case = "Insert Into irma (retweeted_status,tweet_iD,mentioned_ids,created_at,user_id,user_name,full_text,retweet_count,in_reply_to_user_id,in_reply_to_screen_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        self.assertEqual(testing_case,insertTable(cursor,data,"Irma"),"failed at insertTable()")
        cursor.close()
        print("All Test cases pasted for test_insertTable()!")

if __name__ == "__main__":
    unittest.main()