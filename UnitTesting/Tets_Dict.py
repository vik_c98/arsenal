import unittest
from Dict import media_dict,gov_dict


class Test_Dict(unittest.TestCase):

    def test_media_dict(self):
        m = media_dict()
        self.assertTrue(isinstance(m,dict),"media_dict() is not a dictionary!")
        self.assertTrue(m["ABC"], "ABC should be in the dictionary, but is not!")
        try:
            testcase = m["NBA"]
            print("Test fail at failing case!")
        except KeyError:
            print("All test passes for media dict, including failing case!")


    def test_gov_dict(self):
        g = gov_dict()
        self.assertTrue(isinstance(g,dict),"gov_dict() is not a dictionary!")
        self.assertTrue(g["realDonaldTrump"], "realDonaldTrump should be in the dictionary, but is not!")
        try:
            testcase = g["Obama"]
            print("Test fail at failing case!")
        except KeyError:
            print("All test passes for government dict, including failing case!")

if __name__ == "__main__":
    unittest.main()