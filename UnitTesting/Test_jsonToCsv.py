import unittest
from jsonToCsv import calculateFileSize,calcsvlines,cleanup,changeformatcsv
from  autodelete import delete
import json
from csvToMySQL import collectHeaderRow

class Test_jsonToCsv(unittest.TestCase):

    def test_calculateFileSize(self):
        self.assertEqual(278,calculateFileSize("Test_case3.txt"))
        self.assertEqual(84097,calculateFileSize("Test_case4.txt"))
        print("All Test cases pasted for test_calculateFileSize !")

    def test_calcsvlines(self):
        self.assertEqual(1331,calcsvlines("Test_case1.csv"), "Test failed at test case 1")
        self.assertEqual(100,calcsvlines("Test_case2.csv"),"Test failed at test case 2")
        print("All Test cases pasted for test_calcsvlines !")

    def test_cleanup(self):
        delete("Irma")
        cleanup("Irma")
        f = open("Irmacleanup.json","r")
        data = json.load(f)
        self.assertEqual(10,len(data[0].keys()),"The number of attributes of Json does not match!")
        self.assertEqual(768135,calculateFileSize("Irmacleanup.json"),"Size of file of Json does not match!")
        f.close()
        print("All Test cases pasted for test_cleanup")

    def test_changeformatcsv(self):
        changeformatcsv("Irma.json")
        self.assertEqual(10,len(collectHeaderRow("/Users/hopo/Desktop/FIT3162/Arsenal/arsenal/arsenal/Irma.csv")),"The number of attributes of csv does not match!")
        self.assertEqual(768136,calcsvlines("Irmacleanup.csv"),"Size of file of csv does not match!")

        print("All Test cases pasted for test_changeformatcsv!")



if __name__ == "__main__":
    unittest.main()