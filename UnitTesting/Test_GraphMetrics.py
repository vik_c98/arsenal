import unittest
from jsonToCsv import calculateFileSize


class Test_GraphMetrics(unittest.TestCase):

    def test_Top5MedText(self):
        file = open("/Users/hopo/Desktop/FIT3162/Arsenal/arsenal/arsenal/GraphMetricMedia.txt","r")
        self.assertEqual(357,calculateFileSize("/Users/hopo/Desktop/FIT3162/Arsenal/arsenal/arsenal/GraphMetricMedia.txt"),"The number of records doesn't match!")
        for record in file:
            self.assertEqual(2,len(record.split(',')),"The numebr of attributes doesn't match!")
            break
        print("All test cases passed for test_Top5MedText()!")

    def test_Top5GovText(self):
        file = open("/Users/hopo/Desktop/FIT3162/Arsenal/arsenal/arsenal/GraphMetricGovernment.txt","r")
        self.assertEqual(540,calculateFileSize("/Users/hopo/Desktop/FIT3162/Arsenal/arsenal/arsenal/GraphMetricGovernment.txt"),"The number of records doesn't match!")
        for record in file:
            self.assertEqual(2,len(record.split(',')),"The numebr of attributes doesn't match!")
            break
        print("All test cases passed for test_Top5GovText()!")




if __name__ == "__main__":
    unittest.main()