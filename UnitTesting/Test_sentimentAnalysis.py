import unittest
from sentimentAnalysis import indexPos,fetchdata,removeURL,filterTweets,extractMedia_text,extractGov_text,sentimentanalysis
from Extractdata import fetch
from csvToMySQL import collectHeaderRow


class Test_sentimentAnalysis(unittest.TestCase):

    def test_indexPos(self):
        data = fetch("Irma")
        header = collectHeaderRow("Irma.csv")
        index_of_fulltext,index_of_retweetedstatus,index_of_user_screenname,index_of_metioned_ids = indexPos(data)
        self.assertEqual("full_text",header[index_of_fulltext],"Test fail at index, index_of_fulltext ")
        self.assertEqual("retweeted_status",header[index_of_retweetedstatus],"Test fail at index, index_of_retweetedstatus ")
        self.assertEqual("user_name",header[index_of_user_screenname],"Test fail at index, index_of_user_screenname ")
        self.assertEqual("mentioned_ids",header[index_of_metioned_ids],"Test fail at index, index_of_metioned_ids ")
        print("All test passes for test_indexPos()!")

    def test_fetchdata(self):
        data = fetchdata("Irma")
        self.assertEqual(4,len(data[0]),"The number of length in each record doesn't match!")
        self.assertEqual(768135,len(data),"The number of records doesn't match!")
        print("All test passes for test_fetchdata()!")

    def test_removeURL(self):
        test_case = [["Watching Harvey's Moisture coming towards Long Island. Which is good beach day? Next week eyes on #HurricaneIrma https://t.co/1JPa1LdVIh https://t.co/HndHuXmRMW",123,456]]
        self.assertEqual(removeURL(test_case)[0][0],"Watching Harvey's Moisture coming towards Long Island. Which is good beach day? Next week eyes on #HurricaneIrma    ","Test case failed at removing URL! ")
        test_case2 = [["Watching Harvey's Moisture coming towards Long Island. Which is good beach day? Next week eyes on #HurricaneIrma",123,456]]
        self.assertListEqual(removeURL(test_case2),test_case2,"Test case failed when there are no URLs in the input!")
        print("All test passes for test_removeURL()!")

    def test_filterTweets(self):
        data = fetchdata("Irma")
        filtered_tweet,total_originallTweets = filterTweets(data)
        self.assertEqual(3,len(filtered_tweet[0]),"The number of attributes doesn't match!")
        self.assertEqual(207985,len(filtered_tweet),"The number of original tweets doesn't match!")
        print("All test passes for test_filterTweets()")

    def test_extractMedia_text(self):
        Fulltexts = fetchdata("Irma")
        originialTweets, total_original = filterTweets(Fulltexts)
        nonRT_mediaTweets, total_mediaOriginal = extractMedia_text(originialTweets)
        self.assertEqual(6839,total_mediaOriginal,"The number of media original tweets doesn't match!")
        self.assertEqual(2,len(nonRT_mediaTweets[0]),"The number of attribute doesn't match!")
        print("All test passes for test_extractMedia_text()")

    def test_extractGov_text(self):
        Fulltexts = fetchdata("Irma")
        originialTweets, total_original = filterTweets(Fulltexts)
        nonRT_govTweets, total_govoriginal = extractGov_text(originialTweets)
        self.assertEqual(1321, total_govoriginal, "The number of media original tweets doesn't match!")
        self.assertEqual(2, len(nonRT_govTweets[0]), "The number of attribute doesn't match!")
        print("All test passes for test_extractGov_text()")

    def test_sentimentanalysis(self):
        pos_n,neg_n = sentimentanalysis([["Good",1],["Bad",2],["Worse",3],["Love",4]])
        self.assertEqual(2,pos_n,"The number of positive words doesn't match!")
        self.assertEqual(2,neg_n,"The number of negative words doesn't match!")
        pos_n1,neg_n1 = sentimentanalysis([["hate",5],["Bad",6],["Worse",7],["awful",8]])
        self.assertEqual(4,neg_n1,"The number of negative words doesn't match!")
        print("All test passes for test_sentimentanalysis()")


if __name__ == "__main__":
    unittest.main()