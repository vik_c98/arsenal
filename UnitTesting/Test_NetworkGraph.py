import unittest
from NetworkGraph import indexPos,returnTopReplied
from Extractdata import fetch
from csvToMySQL import collectHeaderRow
from jsonToCsv import calculateFileSize


class Test_NetworkGraph(unittest.TestCase):

    def test_indexPos(self):
        data = fetch("Irma")
        header = collectHeaderRow("Irma.csv")
        index_of_user_name,index_of_user_id,index_of_in_reply_to_user_id,index_of_in_reply_to_screen_name = indexPos(data)
        self.assertEqual("user_name",header[index_of_user_name],"Test fail at index, index_of_user_name ")
        self.assertEqual("user_id",header[index_of_user_id],"Test fail at index, index_of_user_id ")
        self.assertEqual("in_reply_to_user_id",header[index_of_in_reply_to_user_id],"Test fail at index, index_of_in_reply_to_user_id ")
        self.assertEqual("in_reply_to_screen_name",header[index_of_in_reply_to_screen_name],"Test fail at index, index_of_in_reply_to_screen_name ")
        print("All test passes for test_indexPos()!")


    def test_returnTopReplied(self):
        data = fetch("Irma")
        test_media = ['weatherchannel', 'NHC_Atlantic', 'CNN', 'FoxNews', 'ABC']
        test_gov = ['realDonaldTrump', 'marcorubio', 'NASA', 'FLOTUS', 'SenSchumer']
        self.assertListEqual(returnTopReplied("media",data),test_media,"Test fail at media top relied!")
        self.assertListEqual(returnTopReplied("government",data),test_gov,"Test fail at government top relied!")
        self.assertEqual(returnTopReplied("sport",data),None,"Test fail at failing case!")
        print("All test passes test_returnTopReplied()!")

    def test_createNetworkgraphtxt(self):
        self.assertEqual(275,calculateFileSize("/Users/hopo/Desktop/FIT3162/Arsenal/arsenal/arsenal/txts/CNN.txt"),"Test fail at size of the CNN related text file")
        self.assertEqual(37,calculateFileSize("/Users/hopo/Desktop/FIT3162/Arsenal/arsenal/arsenal/txts/SenTedCruz.txt"),"Test fail at size of the SenTedCruz related text file")
        print("All test passes test_createNetworkgraphtxt()!")

if __name__ == "__main__":
    unittest.main()