import unittest
from Extractdata import fetch
from jsonToCsv import calcsvlines


class Test_jsonToCsv(unittest.TestCase):

    def test_fecth(self):
        data = fetch("Irma")

        count_fordata = 0
        for record in data:
            if count_fordata == 0:
                self.assertEqual(len(record),10,"Number of attributes doesn't match in fetch()!")
            count_fordata+=1

        self.assertEqual(count_fordata,calcsvlines("Irmacleanup.csv"),"Number of records doesn't macth!")

        try:
            data = fetch("errorIrma")
            print("fail at failing case!")
        except StopIteration:
            print("All Test passes, including failing case!")

if __name__ == "__main__":
    unittest.main()