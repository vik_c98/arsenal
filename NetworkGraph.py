from Extractdata import fetch
import networkx as nx
import matplotlib
# matplotlib.use('tkagg')
import matplotlib.pyplot as plt
from Dict import *
import heapq
from os import makedirs,path


"""
code in the url: https://www.youtube.com/watch?v=G4DdK2zth3I&list=PLbu9W4c-C0iC-f1ByMHSa07OSgEQQ-rAk&index=5 has been taken into consideration
"""

"""
This script is to create the network graph with the help of the Networkx package
"""

def indexPos(itr):
    """
    This function is used to return the indexes of the needed columns from database.
    :param: data iterator of the data in the database.
    :raise:
    :pre: The data iterator exists (or has been declared before).
    :return: return the indexes of the needed columns from database.
    :Time complexity: O(n), where n denote the number of line in the csv file.
    """
    headerRow = []
    count = 0
    index_of_user_name = 0
    index_of_user_id = 0
    index_of_in_reply_to_user_id = 0
    index_of_in_reply_to_screen_name = 0
    for index in itr:
        if count == 0:
            headerRow = index
            count +=1
        else:
            break
    for i in range(len(headerRow)):
        if headerRow[i] == "user_name":
            index_of_user_name = i
        elif headerRow[i] == "user_id":
            index_of_user_id = i
        elif headerRow[i] == "in_reply_to_user_id":
            index_of_in_reply_to_user_id = i
        elif headerRow[i] == "in_reply_to_screen_name":
            index_of_in_reply_to_screen_name = i

    return index_of_user_name,index_of_user_id,index_of_in_reply_to_user_id,index_of_in_reply_to_screen_name




def returnTopReplied(choiceOfcategory, data):
    """
    This function is used to return the top 5 "replied to" users in the database with specified choice.
    :param: choice related to the users (government or media), and the corresponding data in the database.
    :raise:
    :pre: The choice has been specified and the data iterator of the data in the database exists.
    :return: return the indexes of the needed columns from database.
    :Time complexity: O(n), where n denote the number of line of tweet related data in the database.
    """
    if choiceOfcategory == "government":
        dictofGov = gov_dict()

        index_of_user_name, index_of_user_id, index_of_in_reply_to_user_id, index_of_in_reply_to_screen_name = indexPos(data)
        count = 0
        for tweet in data:
            if count != 0:
                if tweet[index_of_in_reply_to_user_id] != "" and tweet[index_of_user_id] != tweet[index_of_in_reply_to_user_id]:
                    if checkexist(dictofGov,tweet[index_of_user_name]) != 0 and checkexist(dictofGov,tweet[index_of_in_reply_to_screen_name]) !=0:
                        dictofGov[tweet[index_of_user_name]] += 1
                        dictofGov[tweet[index_of_in_reply_to_screen_name]] += 1
                    elif checkexist(dictofGov,tweet[index_of_user_name]) != 0 and checkexist(dictofGov,tweet[index_of_in_reply_to_screen_name]) ==0:
                        dictofGov[tweet[index_of_user_name]] += 1
                    elif checkexist(dictofGov,tweet[index_of_user_name]) == 0 and checkexist(dictofGov,tweet[index_of_in_reply_to_screen_name]) !=0:
                        dictofGov[tweet[index_of_in_reply_to_screen_name]] += 1
            count += 1
        Top5Gov = heapq.nlargest(5, dictofGov, key=dictofGov.get)


        return Top5Gov
    elif choiceOfcategory == "media":
        dictofMed = media_dict()
        index_of_user_name, index_of_user_id, index_of_in_reply_to_user_id, index_of_in_reply_to_screen_name = indexPos(data)
        count = 0
        for tweet in data:
            if count != 0:
                if tweet[index_of_in_reply_to_user_id] != "" and tweet[index_of_user_id] != tweet[index_of_in_reply_to_user_id]:
                    if checkexist(dictofMed,tweet[index_of_user_name]) != 0 and checkexist(dictofMed,tweet[index_of_in_reply_to_screen_name]) !=0:
                        dictofMed[tweet[index_of_user_name]] += 1
                        dictofMed[tweet[index_of_in_reply_to_screen_name]] += 1
                    elif checkexist(dictofMed,tweet[index_of_user_name]) != 0 and checkexist(dictofMed,tweet[index_of_in_reply_to_screen_name]) ==0:
                        dictofMed[tweet[index_of_user_name]] += 1
                    elif checkexist(dictofMed,tweet[index_of_user_name]) == 0 and checkexist(dictofMed,tweet[index_of_in_reply_to_screen_name]) !=0:
                        dictofMed[tweet[index_of_in_reply_to_screen_name]] += 1
            count += 1
        Top5Med = heapq.nlargest(5, dictofMed, key=dictofMed.get)
        return Top5Med

def createNetworkgraphtxt(choiceOfCategory,choiceOfDataset):
    """
    This function is used to create the text files for the top 5 replied specified by choice(government or media) users, the name used to bulid the network graph are screen name due to the twitter restriction (ease for our analysis).
    And we eliminated the cases that users reply to theirselves.
    :param: choice related to the users (government or media).
    :raise:Error raise when the choice specified by user is invalid.
    :pre: The choice has been specified.
    :return: The 5 text files for the top 5 replied specified(government or media) users.
    :Time complexity: O(n), where n denote the number of line of tweet related data in the database.
    """
    choiceOfCategory = choiceOfCategory.lower()
    if choiceOfCategory == "government" :
        gov_txt = []
        data = fetch(choiceOfDataset)
        Top5Gov = returnTopReplied(choiceOfCategory, data)
        for gov in Top5Gov:
            f = open(gov + ".txt", "w+")
            index_of_user_name, index_of_user_id, index_of_in_reply_to_user_id, index_of_in_reply_to_screen_name = indexPos(
                data)
            count = 0
            for tweet in data:
                if count != 0:
                    if tweet[index_of_in_reply_to_user_id] != "" and tweet[index_of_user_id] != tweet[index_of_in_reply_to_user_id]:
                        if checkexist(gov_dict(), tweet[index_of_user_name]) == 1 or checkexist(gov_dict(), tweet[index_of_in_reply_to_screen_name]) == 1:
                            if tweet[index_of_user_name] == gov or tweet[index_of_in_reply_to_screen_name] == gov:
                                UserName = str(tweet[index_of_user_name])
                                in_reply_to_screen_name = str(tweet[index_of_in_reply_to_screen_name])
                                NewUserName = UserName.replace(" ", "_")
                                NEW_in_reply_to_screen_name = in_reply_to_screen_name.replace(" ", "_")
                                stringToWrite = NewUserName + " " + NEW_in_reply_to_screen_name + "\n"
                                f.write(stringToWrite)
                count += 1
            gov_txt.append(gov+".txt")
            f.close()
        return gov_txt

    elif choiceOfCategory == "media" :
        Med_txt = []
        data = fetch(choiceOfDataset)
        Top5Med = returnTopReplied(choiceOfCategory, data)
        for Med in Top5Med:
            f = open(Med + ".txt", "w+")
            index_of_user_name, index_of_user_id, index_of_in_reply_to_user_id, index_of_in_reply_to_screen_name = indexPos(
                data)
            count = 0
            for tweet in data:
                if count != 0:
                    if tweet[index_of_in_reply_to_user_id] != "" and tweet[index_of_user_id] != tweet[index_of_in_reply_to_user_id]:
                        if checkexist(media_dict(), tweet[index_of_user_name]) == 1 or checkexist(media_dict(), tweet[index_of_in_reply_to_screen_name]) == 1:
                            if tweet[index_of_user_name] == Med or tweet[index_of_in_reply_to_screen_name] == Med:
                                UserName = str(tweet[index_of_user_name])
                                in_reply_to_screen_name = str(tweet[index_of_in_reply_to_screen_name])
                                NewUserName = UserName.replace(" ", "_")
                                NEW_in_reply_to_screen_name = in_reply_to_screen_name.replace(" ", "_")
                                stringToWrite = NewUserName + " " + NEW_in_reply_to_screen_name + "\n"
                                f.write(stringToWrite)
                count += 1
            Med_txt.append(Med+".txt")
            f.close()
        return Med_txt
    else:
        raise StopIteration("The choice must be between goverment and media!!!")


def createAnewdirectory(newpath):
    #code contained in this URL has taken into consideration
    #https://stackoverflow.com/questions/11373610/save-matplotlib-file-to-a-directory
    try:
        makedirs(newpath)
    except OSError as oserr:
        if oserr.errno == 17 and path.isdir(newpath):
            pass
        else:
            raise




def createNetworkgraph(choiceOfDataset):
    """
    This function is used to build 5 network graphs with the structure (user (reply to) user) using the text files that have been constructed before.
    :param: choice related to the users (government or media).
    :raise:
    :pre: The 10 text files used to build network graph exist.
    :return: The 10 network graphs (5 for Top 5 replied government user graph, and 5 for Top 5 replied media user graph.
    :Time complexity: O(n), since both for loops will loop through constant time and the function nx.read_edgelist() has O(n) complexity, so in total O(n), wher n denotes number of lines in the text file we have created by createNetworkgraphtxt().
    """

    allGraph=["media","government"]
    outputdirectory =  str(choiceOfDataset)+ "Findings"
    createAnewdirectory(outputdirectory)
    for items in allGraph:
        itemsfile = createNetworkgraphtxt(items,choiceOfDataset)
        for item in itemsfile:
            figsize = plt.rcParams["figure.figsize"]
            figsize[0] = 6
            figsize[1] = 6
            g = nx.read_edgelist(item, create_using=nx.Graph(), nodetype=str)
            # print(nx.info(g))
            plt.axis('off')
            nx.draw_networkx(g, pos=nx.fruchterman_reingold_layout(g), font_size=1, with_labels=True, node_size=10,
                             width=0.08)
            plt.savefig('{}/'.format(outputdirectory)+item[0:len(item)-4] + '.png', dpi=1200)
            g.clear()
            plt.clf()
            plt.close()




