# author: YANG CUI 27341919 and KaPo Ho 26989433
# This file contains the codes
"""
This script is to clean up the json we got after hydration on Twitter IDs.
It reads the old json file in and dumps the new cleaned up version to a new json file called "new.json"

"""
import json
import csv
from extension import *
from autodelete import *


def calculateFileSize(filename):
    """
    function calculateFileSize returns the number of lines in a given txt or json file  as int to the user.
    :param filename: The file whose size we are interested in
    :raise:
    :pre: filename exists in the current directory and its either a txt file or a json file
    :return: the size of the file returned to user
    :time complexity : O(n) n being the number of lines in the given file
    """
    numberOfLines=0
    f=open(filename,"r")
    for lines in f:
        numberOfLines+=1
    f.close()
    return numberOfLines


def calcsvlines(filename):
    """
    function calcsvlines returns the number of lines in a given csv file as int to the user.
    :param filename: The csv file whose size we are interested in
    :raise:
    :pre: filename exists in the current directory and it's a csv file
    :return: the size of the file returned to user
    :time complexity : O(n) n being the number of lines in the given csv file
    """
    input_file = open(filename, "r+")
    reader_file = csv.reader(input_file)
    value = len(list(reader_file))
    input_file.seek(0)
    input_file.close()
    return value

def cleanup(filename):
    """
    function cleanup reads in the resulting json file after we feed the original json into the hydrator and extracts 10
    key attributes that we actually need and save it into a new json file called nameofdatabase+cleanup.json
    :param filename: the resulting json file after we feed the original json into the hydrator
    :raise:
    :pre: filename exists in the current directory and
    :return: the new json file containing only the 10 key attributes that we actually need
    :time complexity : O(n) n being the number of lines in the given json file file
    """
    # printed = False
    # pre, ext = checkextension(filename)
    # print(ext!=".json")
    # if pre == filename:
    #     filename=os.rename(filename, pre + ".txt")
    pre, ext = checkextension(filename)
    if check_fileexist(filename)==False:
        # pre, ext = os.path.splitext(filename+".json")
        pre,ext = checkextension(filename+".json")
        os.rename(filename+".json", pre)

    # pre, ext = checkextension(filename)
    elif check_fileexist(filename):
        addextension(filename)
        filename = filename+".json"

    with open(filename) as f:
        fileSize = calculateFileSize(filename)
        counter = 0
        f1=open(pre + "cleanup.json","a+")
        for lines in f:

            Temp = dict()
            data = json.loads(lines)
            key = "retweeted_status"
            if key in data:     #if it is true then it is a retweet, else not a retweet.
                Temp["retweeted_status"] = True
            else:
                Temp["retweeted_status"] = False

            Temp["tweet_iD"] = data["id"]
            Temp["mentioned_ids"] = data["entities"]["user_mentions"]
            Temp["created_at"] = data["created_at"]
            Temp["user_id"] = data["user"]["id"]
            Temp["user_name"] = data["user"]["screen_name"]
            Temp["full_text"] = data["full_text"]
            Temp["real_name"] = data["user"]["name"]
            Temp["in_reply_to_user_id"]=data["in_reply_to_user_id"]
            Temp["in_reply_to_screen_name"]=data["in_reply_to_screen_name"]


            if counter == 0:
                f1.write("[")
            json.dump(Temp, f1)
            if counter != fileSize - 1:
                f1.write(",\r\n")
            if counter == fileSize - 1:
                f1.write("]")
            counter += 1
        f1.close()
        f.close()






#changeformat to csv
def changeformatcsv(filename):
    """
    function changeformatcsv reads in the resulting json file after we called cleanup function, the file that only contains the 10
    key attributes that we need and returns a csv file containing the same info called nameofdatabase+cleanup.csv.
    :param None since this will act as the main function for this entire file
    :raise:
    :pre: filename exists in the current directory and its already cleaned up using the clean up function
    :return: returns a csv file containing the same info called new.csv.
    :time complexity : O(n) n being the number of lines in the given json file file
    """
    # open files
    pre, ext = checkextension(filename)
    sourcefile = open(pre + "cleanup.json","rU")
    outputfile = open(pre+ "cleanup.csv","w")
    outputWriter= csv.writer(outputfile)
    # flag to control header writing
    needs_header=True

    # load data from new.json
    json_data = json.load(sourcefile)
    # print(json_data)
    for items in json_data:
        # print("\n")
        # for attributes in items:
        row_array=[]
        header_row = []
        # row_array.append(items)
        for attributes in items:
            if needs_header == True:

                header_row.append(attributes)

            row_array.append(items[attributes])
        if needs_header == True:
            outputWriter.writerow(header_row)
            # print(header_row)
            needs_header = False
        outputWriter.writerow(row_array)

    sourcefile.close()
    outputfile.close()

def jsonToCsv_main(filename):
    delete(filename)
    cleanup(filename)
    changeformatcsv(filename)
    # print(calculateFileSize(filename + "cleanup.json"))
    # print(calcsvlines(filename + "cleanup.csv"))

# main("Irma")

# filename = "Irma.json"
# pre, ext = os.path.splitext(filename)
# os.rename(filename,pre)



# print(calculateFileSize("Irmacleanup.json"))
# delete("Irma")
# cleanup("Irma")
# changeformatcsv()
# print(calculateFileSize("new.json"))
# print(calcsvlines("Irmacleanup.csv"))
# jsonToCsv_main("Irma")
