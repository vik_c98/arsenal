import csv
import mysql.connector
from mysql.connector import errorcode

#https://www.youtube.com/watch?v=1ji8lqiBJe0 (database connection ready for reference)

"""
This script is to insert all the data in csv from dataset into the database (MySQL)
"""
from extension import *

def collectHeaderRow(filename):
    """
    This function collect the first row (header row) of the dataset in csv form.
    :param:a valid file name
    :raise:
    :pre: The csv file exist
    :return: the first row of the data from dataset, which is the header row.
    :Time complexity: O(n), where n denote the number of line in the csv file.
    """
    pre, ext = checkextension(filename)
    input_file = open(pre+"cleanup.csv", "r+")
    #only need to input Irma or Harvey
    reader_file = csv.reader(input_file)
    HeaderRow = []
    for rows in reader_file:
        input_file.close()
        return rows





def returnIterator(filename):
    """
    This function return the data iterator for the dataset in the csv file.
    :param:filename
    :raise:
    :pre: The filename is a valid csv file
    :return: the data iterator for the dataset in the csv file
    :Time complexity: O(n), where n denote the number of line in the csv file.
    """
    input_file = open(filename, "r+")
    reader_file = csv.reader(input_file)
    return reader_file

# headerRow= collectHeaderRow()




def dropTable(curs,filename):
    """
    This function is used to initialise the drop table command with specified table name in string formatt and use the cursor to execute the drop table command in database.
    :param:The cursor
    :raise:
    :pre: The cursor has been declared
    :return: None
    :Time complexity: O(1)
    """
    pre, ext = checkextension(filename)
    dropt = "DROP TABLE " + pre.lower()
    curs.execute(dropt)
    return dropt ##########Maybe error



def createTable(curs,filename):
    """
    This function is used to intialise the create table command with certain number of attributes in string formatt and use the cursor execute the create table command in the database.
    :param:The cursor
    :raise:
    :pre: The cursor has been declared
    :return: None
    :Time complexity: O(n)
    """
    headerRow = collectHeaderRow(filename)
    pre, ext = checkextension(filename)
    createTable = "CREATE TABLE " + pre.lower() + " ("
    for i in range(len(headerRow)):
        if headerRow[i] == "created_at":
            createTable = createTable + "created_at" + " " + "VARCHAR(255),"
        elif headerRow[i] == "full_text":
            createTable = createTable + "full_text" + " " + "TEXT,"
        elif headerRow[i] == "retweeted_status":
            createTable = createTable + "retweeted_status" + " " + "VARCHAR(255),"
        elif headerRow[i] == "user_name":
            createTable = createTable + "user_name" + " " "VARCHAR(255),"
        elif headerRow[i] == "tweet_iD":
            createTable = createTable + "tweet_iD" + " " + "VARCHAR(255),"
        elif headerRow[i] == "mentioned_ids":
            createTable = createTable + "mentioned_ids" + " " + "TEXT,"
        elif headerRow[i] == "user_id":
            createTable = createTable + "user_id" + " " + "VARCHAR(255),"
        elif headerRow[i] == "real_name":
            createTable = createTable + "real_name" + " " + "VARCHAR(255),"
        elif headerRow[i] == "in_reply_to_user_id":
            createTable = createTable + "in_reply_to_user_id" + " " + "VARCHAR(255),"
        else:
            createTable = createTable + "in_reply_to_screen_name" + " " + "VARCHAR(255),"

    createTable = createTable[:len(createTable) - 1]
    createTable = createTable + ")"
    curs.execute(createTable)
    return createTable

def insertTable(curs,iter,filename):
    """
    This function is used to intialise the insert table command with specified value in string formatt and use the cursor execute the insert table command in the database.
    :param:The cursor and the data iterator
    :raise:
    :pre: The cursor has been declared and the data iterator exist
    :return: None
    :Time complexity: O(n)
    """
    headerRow = collectHeaderRow(filename)
    pre, ext = checkextension(filename)
    insertTable = "Insert Into " + pre.lower() + " ("

    for i in range(len(headerRow)):
        insertTable = insertTable + headerRow[i] + ','
    insertTable = insertTable[:len(insertTable) - 1] + ")"
    insertTable = insertTable + " VALUES " + '('
    for i in range(len(headerRow)):
        insertTable = insertTable + "%s,"

    insertTable = insertTable[:len(insertTable) - 1] + ")"
    count = 0
    for data in iter:
        # print(data)
        if count != 0:
            curs.execute(insertTable, (data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7],data[8],data[9]))
        count += 1
    return insertTable



def buildConnectionDb(filename):
    """
    This function is used to build MySQL database connection with python, and intialize the cursor, data iterator and execute the related drop table command, create table command as well as insert table command.
    :param:None
    :raise:error raise when the MySQL connector errors happened such as wrong user name,wrong password, wrong host name or wrong database name.
    :pre: the corresponding database exist
    :return: None
    :Time complexity: O(n)
    """
    try:
        cnn = mysql.connector.connect(
            user='root',
            password='08110320hjbcy',
            host='localhost',
            database='Arsenal')
        # print("It works!!")

        pre, ext = checkextension(filename)
        cursor = cnn.cursor()
        data = returnIterator(pre+"cleanup.csv")

        dropTable(cursor,filename)
        createTable(cursor,filename)

        insertTable(cursor,data,filename)

        cnn.commit()
        cursor.close()
        cnn.close()
    except mysql.connector.Error as e:
        if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            raise StopIteration("Something is wrong with username or Password")

        elif e.errno == errorcode.ER_BAD_DB_ERROR:
            raise StopIteration("Database Does not exist")
        else:
            raise StopIteration(e)


# buildConnectionDb("Irma")
