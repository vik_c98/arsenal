import mysql.connector
from mysql.connector import errorcode
from csvToMySQL import collectHeaderRow

def fetch(choiceOfData):
    """
    function fetch returns the databases iterator if the connection is built successfully
    :param: the choice of the database
    :raise:
    :pre: mysql.connector package installed and collectHeaderRow function from file csvToMySQL ready to use
    :return: returns the databases iterator if the connection is built successfully else stopIteration with error message
    :time complexity : O(n) n being number of records in the database
    """
    if choiceOfData != "Harvey" and choiceOfData != "Irma":
        raise StopIteration("Invalid choice of data, choice has to be either Harvey or Irma, the first letter should be capitalized!")

    try:
        cnn = mysql.connector.connect(
            user='root',
            password='08110320hjbcy',
            host='localhost',
            database='Arsenal')
        # print("It works!!")

        cursor = cnn.cursor()
        cursor.execute("select * from " + choiceOfData.lower())
        data = cursor.fetchall()
        header = collectHeaderRow(choiceOfData)
        data.insert(0,header)

        cnn.commit()
        cursor.close()
        cnn.close()
        return data
    except mysql.connector.Error as e:
        if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            raise StopIteration("Something is wrong with username or Password")

        elif e.errno == errorcode.ER_BAD_DB_ERROR:
            raise StopIteration("Database Does not exist")
        else:
            raise StopIteration(e)

