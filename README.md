**A simple/easy guide to use the software:**

**MySQL database server needs to be installed via: https://dev.mysql.com/downloads/mysql/**
after successfully installed the server, go ahead and create a localhost and a database called **Arsenal**. 
<br />The team used the following for the database:
            user='root',
            password='08110320hjbcy',
            host='localhost',
            database='Arsenal'

**The software was tested/developed using python version 3.6**
There is no guarantee other versions will be compatible 

**The following Packages/libraries needs to be installed beforehand:**

**1) mysql.connector**
you can install it using the command "pip install mysql-connector-python==2.1.6"
Note: the version of mysql.connector has to be the same as the server, otherwise it wouldn't be compatible.

**2) Networkx**
you can install it using the command "pip install networkx"

**3) NumPy**
you can install it using the command "pip install numpy"

**4) matplotlib**
you can install it using the command "pip install matplotlib==2.0.0"
Note: we have tried other versions, and it seems to us that this is a compatible version. If you go with another version, we can't guarantee it will work!

**5) SciPy**
you can install it using the command "pip install scipy"

**6) Textblob**
you can install it using the command "pip install textblob"

**7) nltk**
you can install it using the command "pip install nltk"
Note you will also have to run the following code in python to download all of nltks data:
>>> import nltk
>>> nltk.download("all")

**8) spaCy**
you can install it using the command "pip install spacy"

**9) gensim**
you can install it using the command "pip install gensim"

**10) tkinter**
you can install it using the command "pip install python-tk"

Note that the datasets have to be in the same directary as the software and they have to be processed using Hydrator prior.

Now to run the software, you simply just go to TwitterArsenalUI.py and hit run. 
This acts as the simple GUI to our software where you can then choose which dataset to run the software on.
It will sponge a control window where two buttons will be displayed: Irma and Harvey.
Users simply click on one of those to run the software. 
All of the results will be in the folder "IrmaFindings" or "HarveyFindings"

