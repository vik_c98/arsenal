import os
#https://stackoverflow.com/questions/2900035/changing-file-extension-in-python



def checkextension(filename):
    """
    function checkextension returns the filename and the extension of a given file to the user
    :param filename  the file we are interested in
    :raise:
    :pre: os library imported
    :return: the filename and the extension of a given file to the user
    :time complexity : O(1)
    """
    pre, ext = os.path.splitext(filename)
    return pre, ext

def addextension(filename):
    """
    function addextension first calls the function checkextension to see if the extension of filename is already .json. If not
    we then change it to .json.
    :param filename  the file we are interested in
    :raise:
    :pre: os library imported and checkextension() ready to use
    :return: changes the extension to .json if its not already
    :time complexity : O(1)
    """
    pre, ext = checkextension(filename)
    if ext != ".json":
        os.rename(filename, pre + ".json")

def check_fileexist(filename):
    try:
        f = open(filename,"r")
        f.close()
        return True
    except FileNotFoundError:
        return False

