import os

def delete(filename):
    """
    function delete first checks if there is already a new.json in the current directory and if there is delete it
    key attributes that we need and returns a csv file containing the same info called new.csv.
    :param None
    :raise:
    :pre: os library imported
    :return: Nothing only delects the file if it exists
    :time complexity : O(1)
    """
    pre, ext = os.path.splitext(filename)
    if os.path.exists(pre + "cleanup.json"):
        os.remove(pre+"cleanup.json")

