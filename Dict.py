def media_dict():
    """
    function media_dict initializes a dict for all the media screen names that we are interested in when called upon
    :param None
    :raise:
    :pre:
    :return: the dict containing all the media screen names that we are interested in
    :time complexity : O(1)
    """
    # To media
    media_dict = {}
    media_dict["ABC"] = 1
    media_dict["CBS"] = 1
    media_dict["NBC"] = 1
    media_dict["Fox"] = 1
    media_dict["MSNBC"] = 1
    media_dict["NBCNews"] = 1
    media_dict["CBCNews"] = 1
    media_dict["TheDailyEdge"] = 1
    media_dict["washingtonpost"] = 1
    media_dict["FOX4"] = 1
    media_dict["kiii3news"] = 1
    media_dict["thehill"] = 1
    media_dict["FoxNews"] = 1
    media_dict["NHC_Atlantic"] = 1
    media_dict["TheEllenShow"] = 1
    media_dict["CBSNews"] = 1
    media_dict["gdimeweather"] = 1
    media_dict["LesterHoltNBC"] = 1
    media_dict["ConvoyofHope"] = 1
    media_dict["abc13houston"] = 1
    media_dict["insideclimate"] = 1
    media_dict["HoustonOEM"] = 1
    media_dict["ACLU"] = 1
    media_dict["CNN"] = 1
    media_dict["NWS"] = 1
    media_dict["dailykos"] = 1
    media_dict["PressTV"] = 1
    media_dict["KXAN_News"] = 1
    media_dict["NWSCorpus"] = 1
    media_dict["CBSDFW"] = 1
    media_dict["Fox26Houston"] = 1
    media_dict["NCCU"] = 1
    media_dict["HBCUDigest"] = 1
    media_dict["wxbrad"] = 1
    media_dict["247NewsHQ"] = 1
    media_dict["ABC7Chicago"] = 1
    media_dict["AP"] = 1
    media_dict["KBTXShel"] = 1
    media_dict["cnnbrk"] = 1
    media_dict["NEweatherHQ"] = 1
    media_dict["ABC7News"] = 1
    media_dict["KNGMusic"] = 1
    media_dict["NWSHouston"] = 1
    media_dict["YouTube"] = 1
    media_dict["FoxBusiness"] = 1
    media_dict["TheNCUA"] = 1
    media_dict["TIME"] = 1
    media_dict["USATODAY"] = 1
    media_dict["nytimes"] = 1
    media_dict["bbcweather"] = 1
    media_dict["WCVB"] = 1
    media_dict["WFTV"] = 1
    media_dict["weatherchannel"] = 1
    media_dict["BN9"]=1
    media_dict["KTSWNews"]=1
    media_dict["wachfox"]=1
    media_dict["WeatherBug"]=1
    return media_dict

def gov_dict():
    """
    function gov_dict initializes a dict for all the government screen names that we are interested in when called upon
    :param None
    :raise:
    :pre:
    :return: the dict containing all the government screen names that we are interested in
    :time complexity : O(1)
    """
    gov_dict = {}
    gov_dict["realDonaldTrump"]=1
    gov_dict["FLOTUS"]=1
    gov_dict["NASA"]=1
    gov_dict["SenSchumer"]=1
    gov_dict["SenShelby"]=1
    gov_dict["SenDougJones"]=1
    gov_dict["lisamurkowski"]=1
    gov_dict["SenDanSullivan"]=1
    gov_dict["SenJonKyl"]=1
    gov_dict["JeffFlake"]=1
    gov_dict["SenTomCotton"]=1
    gov_dict["JohnBoozman"]=1
    gov_dict["SenFeinstein"]=1
    gov_dict["SenKamalaHarris"]=1
    gov_dict["SenBennetCO"]=1
    gov_dict["SenCoryGardner"]=1
    gov_dict["ChrisMurphyCT"]=1
    gov_dict["SenBlumenthal"]=1
    gov_dict["SenatorCarper"]=1
    gov_dict["ChrisCoons"]=1
    gov_dict["SenBillNelson"]=1
    gov_dict["marcorubio"]=1
    gov_dict["SenDavidPerdue"]=1
    gov_dict["SenatorIsakson"]=1
    gov_dict["brianschatz"]=1
    gov_dict["maziehirono"]=1
    gov_dict["MikeCrapo"]=1
    gov_dict["SenatorRisch"]=1
    gov_dict["SenDuckworth"]=1
    gov_dict["SenatorDurbin"]=1
    gov_dict["SenDonnelly"]=1
    gov_dict["SenToddYoung"]=1
    gov_dict["ChuckGrassley"]=1
    gov_dict["joniernst"]=1
    gov_dict["SenPatRoberts"]=1
    gov_dict["JerryMoran"]=1
    gov_dict["SenateMajLdr"]=1
    gov_dict["RandPaul"]=1
    gov_dict["SenJohnKennedy"]=1
    gov_dict["SenAngusKing"]=1
    gov_dict["SenatorCollins"]=1
    gov_dict["ChrisVanHollen"]=1
    gov_dict["SenatorCardin"]=1
    gov_dict["senmarkey"]=1
    gov_dict["SenWarren"]=1
    gov_dict["SenGaryPeters"]=1
    gov_dict["SenStabenow"]=1
    gov_dict["amyklobuchar"]=1
    gov_dict["SenTinaSmith"]=1
    gov_dict["SenHydeSmith"]=1
    gov_dict["SenatorWicker"]=1
    gov_dict["clairecmc"]=1
    gov_dict["RoyBlunt"]=1
    gov_dict["SteveDaines"]=1
    gov_dict["SenatorTester"]=1
    gov_dict["SenatorFischer"]=1
    gov_dict["BenSasse"]=1
    gov_dict["SenCortezMasto"]=1
    gov_dict["SenDeanHeller"]=1
    gov_dict["SenatorShaheen"]=1
    gov_dict["SenatorHassan"]=1
    gov_dict["CoryBooker"]=1
    gov_dict["SenatorMenendez"]=1
    gov_dict["MartinHeinrich"]=1
    gov_dict["SenatorTomUdall"]=1
    gov_dict["SenGillibrand"]=1
    gov_dict["SenatorBurr"]=1
    gov_dict["SenThomTillis"]=1
    gov_dict["SenatorHeitkamp"]=1
    gov_dict["SenJohnHoeven"]=1
    gov_dict["SenSherrodBrown"]=1
    gov_dict["SenRobPortman"]=1
    gov_dict["jiminhofe"]=1
    gov_dict["SenatorLankford"]=1
    gov_dict["RonWyden"]=1
    gov_dict["SenJeffMerkley"]=1
    gov_dict["SenBobCasey"]=1
    gov_dict["SenToomey"]=1
    gov_dict["SenJackReed"]=1
    gov_dict["SenWhitehouse"]=1
    gov_dict["GrahamBlog"]=1
    gov_dict["SenatorTimScott"]=1
    gov_dict["SenatorRounds"]=1
    gov_dict["SenJohnThune"]=1
    gov_dict["SenAlexander"]=1
    gov_dict["BobCorker"]=1
    gov_dict["JohnCornyn"]=1
    gov_dict["SenTedCruz"]=1
    gov_dict["SenOrrinHatch"]=1
    gov_dict["SenMikeLee"]=1
    gov_dict["SenatorLeahy"]=1
    gov_dict["SenatorSanders"]=1
    gov_dict["timkaine"]=1
    gov_dict["MarkWarner"]=1
    gov_dict["PattyMurray"]=1
    gov_dict["SenatorCantwell"]=1
    gov_dict["SenCapito"]=1
    gov_dict["Sen_JoeManchin"]=1
    gov_dict["SenatorBaldwin"]=1
    gov_dict["SenRonJohnson"]=1
    gov_dict["SenatorEnzi"]=1
    gov_dict["SenJohnBarrasso"]=1
    gov_dict["@USAGov"]=1
    return gov_dict


def checkexist(dict,str):
    """
    function checkexist checks if a given screen name is in the dict if yes then return 1 else return 0
    :param dict: the dict for media or for government
    :raise:
    :pre: dict is already to use and the str is of type str
    :return: the dict containing all the government screen names that we are interested in
    :time complexity : O(1)
    """
    try:
        value = dict[str]
        return value
    except:
        return 0

